# polaris-vaniljesaus

![polaris-vaniljesaus](http://static.polarismedia.no/skins/prod/publication/yggdrasil/gfx/pmvanilla.jpg)

**Culinary sauce for all [Polaris Media](http://polarismedia.no) publications.**

> Each publication serve their styles, graphics and custom scripts mixed in this sauce as special local ingredients.

Contribute to improving this sauce by adding your own flavours!

---

## List of ingredients
- 1 gulp
- 100 g sass
- 1 cup postcss
- 1 tablespoon autoprefixer
- 1 tablespoon cssnano
- 500 ml sourcemaps
- 1/2 a bunch of stylelint
- Magic herbs
- Bucket o'sassdoc

**Optional:**

- Breath mint
- Gun powder
- 1 Jolly Roger

## Method
Take the [gulp](https://bitbucket.org/polarismedia/polaris-vaniljesaus/get/174cb525de38.zip) out of the freezer a day before cooking, leave in the fridge.

Use `npm install` in an ovenproof dish, and preheat the oven to 42°C.

Compile all `.scss` powder inside the `src/**/scss/` casserole by using **`gulp styles`**. This will start an organic Sassy process, and chumps of `.css` as well as seasoned sourcemaps crumbs of `.map` will magically appear in the `scr/**/css/` casserole instead.

Make sure you don't spill any source powder by casefully watching the `src/` casserole at all times. Use **`gulp`** to help you keeping an eye on changes in the `.scss` powder, and the `[styles]` process will help you everytime something happens in the casserole.

**Specific dinner guests**

If you want to cook only for a specific guest, use the **`--publication`** argument. For example using **`gulp --publication=altaposten`** will write files only inside the `/altaposten` folder.

**To the oven! (aka production time)**

When you are ready to put things in the oven, raise your Jolly Roger by running the `[prod]` task. This will mash and shrink your dish to better fit the serving plate, and it also adds a dash of `.min` suffix-herbs. When you run the `[prod]` task, you automatically run `[clean:css]` and `[styles]` in the same run.

**Even better:** Use `npm run build` when you are done editing. This will compile all files, and copy them into a `publication` folder. You can drag this folder to the static server (inside `skins/prod` or `skins/test`) without affecting other files.

#### Backseat chef is watching you
If you fail to meet the cooking syntax quality requirements from backseat chef, your cooking method will be interrupted by the `scss-lint` task.

#### Cleaning the sink
For a merciless and hygienic cleaning, use **`gulp clean:css`** to wipe all leftovers in the `scr/**/css/` casseroles.

#### Organize your recipes for others
Use `gulp sassdoc` to carefully take notes of all your recipes and ingredients used for others to see. These notes are stored in the `/docs/` casserole.

## Table setting
The source ingredients inside the `/src/**/scss/` casseroles will be preprocessed dynamically and relative to each source.

### Example 1
`src/publication/voodoo/scss/soup.scss` is served to

`src/publication/voodoo/css/soup.css` and
`src/publication/voodoo/css/soup.min.css` (only with 'prod')

### Example 2
`src/global/scss/grog.scss` is served to

`src/global/css/grog.css` and
`src/global/css/grog.min.css` (only with 'prod')

---

#### Recipe improvements
- Only process files that have changed, or that has an `@import` that have changed ([gulp-changed](https://github.com/sindresorhus/gulp-changed), [gulp-newer](https://github.com/tschaub/gulp-newer) etc.).
- Find a way to extract code to separate stylesheets, i.e. mobile, desktop, critical, fonts ([postcss-extract](https://github.com/nitive/postcss-extract)).
