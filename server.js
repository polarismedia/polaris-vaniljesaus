const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const compression = require('compression');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// Ad Gzip on request
//app.use(compression());

console.log(path.join(__dirname, '/'));
router.use('/', express.static(path.join(__dirname, '/dist/publication/')));

/**
 * START SERVER
 * @type {string | number}
 */
let serverPort = process.env.PORT || 3025;
app.listen(serverPort, () => {
    console.log(`Vaniljesaus app listening on port ${serverPort}!`);
});

let basicPath = '/vaniljesaus';
app.use(basicPath, router);
