requireReady(['jquery'], function($) {

    /*
    ** Functions
    */

    /* Check if an element is empty (or with whitespace/line breaks only) */
    function isEmpty(el) {
        return !$.trim(el.html());
    };

    /* Remove review fields in article (if empty) */
    function checkReviewMeta() {
        if ( $(".ygg-review-meta") ) {
            if (isEmpty($('.ygg-review-meta'))) {
                $('.ygg-review-meta').remove();
            };
        };
    }

    /* Menu controller */
    function menuClickListener() {

        /* When the menu trigger button is clicked */
        $('.ygg-header .menu-trigger').click(function() {

            /* Show the modal if it's not visible */
            if ( $(this).attr('aria-pressed') === 'false' ) {
                $(this).attr('aria-pressed', 'true');
                $('html').addClass('modal-active');
                $('.ygg-header .menu-trigger .label').text('Lukk');

                /* Hide the modal if it's visible */
            } else if ( $(this).attr('aria-pressed') === 'true' ) {
                $(this).attr('aria-pressed', 'false');
                $('html').removeClass('modal-active');
                $('.ygg-header .menu-trigger .label').text('Meny');
            }
        });
    }

    /* Scroll to top */
    function scrollTrigger() {
        /* When the scroll-to-top button is clicked in the modal view */
        $('.ygg-modal .scroll-trigger').click(function() {
            $('html, body').animate({
                scrollTop: 0
            }, 360);
        });
    }

    /* Factboxes */
    function factBoxTweak() {
        /* Add classes to factboxes */
        $(".ygg-base-article .article-body .factbox")
        .addClass("minimized")
        .append('<a class="minimize"></a>')
        .find(".smallHeader").append('<a class="expand"></a>');

        /* Click factbox title to toggle state */
        $(".ygg-base-article .article-body .factTitle").click(function(){
            $(this).parent().toggleClass("minimized maximized");
        });

        /* Bind state toggler to dynamically created minimize element */
        $("body").on("click", ".ygg-base-article .article-body .factbox .minimize", function() {
            $(this).parent().toggleClass("minimized maximized");
        });
    }

    /* Article age */
    function articleAge() {
        if ($('.dateline .published').length < 1) return;
        var page_date = $('.dateline .published').attr('datetime');
        page_date = page_date.split(' ')[0];
        page_date = page_date.split('-');

        function calculateAge(birthday) {
            var ageDifMs = new Date().getTime() - birthday.getTime();
            var ageDate = new Date(ageDifMs);
            return ageDate.getUTCFullYear() - 1970 < 1 ? 0 : Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        function intToString(int) {
            var tall = ['null', 'ett', 'to', 'tre', 'fire', 'fem', 'seks', 'sju', 'åtte', 'ni', 'ti', 'elleve'];
            return tall[int] !== undefined ? tall[int] : ''+int;
        }

        var age = calculateAge(new Date(page_date[0],page_date[1],page_date[2]));

        if (age >= 1) {
            var html = '<span class="age">Artikkelen er mer enn ' + intToString(age) + ' år gammel</span>';
            $('.dateline').append(html);
        }
    };

    /* Parallax images (feature article) */
    function parallaxImage() {
        $('.media.bgfixed').each(function() {
            $(this).prepend('<div class="parallax"></div>');
            var path = $(this).find('.responsiveImage').attr('data-src');
            path = path.replace(/w\{width\}/, 'w1980');
            $(this).find('.parallax').css('background-image', 'url(' + path + ')');
        });
    }

    /*
    ** Let's go
    */

    checkReviewMeta();
    menuClickListener();
    scrollTrigger();
    factBoxTweak();
    articleAge();
    parallaxImage();
});
