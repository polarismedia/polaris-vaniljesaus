var gulp         = require('gulp');             // Aw ye
var sass         = require('gulp-sass');        // Sass
var sourcemaps   = require('gulp-sourcemaps');  // Sourcemaps
var postcss      = require('gulp-postcss');     // PostCSS
var autoprefixer = require('autoprefixer');     // - Autoprefixer
var cssnano      = require('cssnano');          // - Minify
var reporter     = require('postcss-reporter'); // - Log messages from PostCSS
var syntax_scss  = require('postcss-scss');     // - Scss-syntax for linting
var path         = require('path');             // Node path
var del          = require('del');              // Node del
var rename       = require('gulp-rename');      // Rename files and folders
var stylelint    = require('stylelint');        // Lint the stylesheets
var sassdoc      = require('sassdoc');          // Document the shit out of this thing
var size         = require('gulp-size');        // Show file sizes in console
var plumber      = require('gulp-plumber');     // Prevent errors from breaking pipes
var chalk        = require('chalk');            // Nice console colors


// Task [scss-lint]
gulp.task("scss-lint", function() {

	// Stylelint config rules
	var stylelintConfig = {
		"rules": {
			"block-no-empty": true,
			"color-no-invalid-hex": true,
			"declaration-colon-space-after": "always",
			"declaration-colon-space-before": "never",
			"function-comma-space-after": "always",
			"media-feature-colon-space-after": "always",
			"media-feature-colon-space-before": "never",
			"media-feature-name-no-vendor-prefix": true,
			"max-empty-lines": 3,
			"number-leading-zero": "always",
			"number-no-trailing-zeros": true,
			"property-no-vendor-prefix": true,
			"declaration-block-trailing-semicolon": "always",
			"selector-list-comma-space-before": "never",
			"string-quotes": "double",
			"value-no-vendor-prefix": true
			// "declaration-block-no-duplicate-properties": true,
			// "block-opening-brace-space-before": "always",
			// "block-no-single-line": true,
			// "selector-list-comma-newline-after": "always",
		}
	};

	var processors = [
		stylelint(stylelintConfig),
		reporter({ clearMessages: true, throwError: true })
	];

	return gulp.src(['src/**/*.scss'])
		.pipe(postcss(processors, { syntax: syntax_scss }));
});





// Task: [clean:css]
gulp.task('clean:css', function () {
	console.log(chalk.yellow.bold('*** Emptying the /dist folder... ***'));
	return del(['src/**/css/', 'dist/']);
});


// Task [copy]
gulp.task('copy', function() {
	console.log(chalk.yellow.bold('*** Copying all stylesheets and sourcemaps to the /dist folder... ***'));
	return gulp.src('src/**/css/*.*')
	.pipe(size({
		gzip: true,
		showFiles: true,
		showTotal: false
	}))
	.pipe(gulp.dest('./dist/'));
});

// Task: [sassdoc]
gulp.task('sassdoc', function () {
	var options = {
		dest: 'docs',
		verbose: true,
		display: {
			watermark: false
		},
		groups: {
			'undefined': 'Base',
			'typography': 'Typography',
			'cards': 'Cards',
			'form-control': 'Forms',
			'colors-and-styles': 'Colors & styles'
		},
		basePath: 'https://github.com/trollsyn/polaris-vaniljesaus/blob/master/src/yggdrasil',
	};

	// Document Yggdrasil core only, and not each publications bundle.
	return gulp.src('src/yggdrasil/**/*.scss')
	.pipe(sassdoc(options));
});


// Task: [styles]
gulp.task('styles', gulp.series(['scss-lint', function() {
	console.log(chalk.yellow.bold('*** Compiling stylesheets and sourcemaps... ***'));

	// Collect --arguments from CLI
	// --publication=altaposten
	// args.publication => altaposten
	var args = require('yargs').argv;

	// Default target for *.scss files
	var source = 'src/**/*.scss';

	// Unless given argument --publication=
	if (args.publication && args.publication.length > 0) {
		source = 'src/publication/' + args.publication + '/**/*.scss';
	}

	// Now we decided on the source path
	return gulp.src(source, { base: 'src' })

		.pipe(plumber())

		// Init sourcemaps
		.pipe(sourcemaps.init())

		// Start preprocessing
		.pipe(sass({ outputStyle: 'expanded' }))

		// Autoprefix
		.pipe(postcss([autoprefixer({ browsers: ['defaults'] })]))

		// Show file sizes
		.pipe(size({
			gzip: true,
			showFiles: true,
			showTotal: false
		}))

		// Write external sourcemap file
		.pipe(sourcemaps.write('.'))

		// Traverse one folder up and inside the /css folder instead of /scss
		.pipe(rename(function(filepath) { filepath.dirname = path.dirname(filepath.dirname) + '/css/'; }))

		// Destination for dev styles
		.pipe(gulp.dest('src'))
}]));

// Task [styles:build]
gulp.task('build', gulp.series(['clean:css', 'styles', function () {
	console.log(chalk.yellow.bold('*** Minifying all compiled stylesheets for production... ***'));
	return gulp.src('src/**/*.css', { base: 'src' })

	// Add .min suffix
		.pipe(rename({ suffix: '.min' }))

		// Minify
		.pipe(postcss([cssnano({ calc: { precision: 2 }, discardComments: { removeAll: true } })]))

		.pipe(size({
			gzip: true,
			showFiles: true,
			showTotal: false
		}))

		// Destination
		.pipe(gulp.dest('src'))
}]));



// Task: [default]
gulp.task('default', gulp.series(['styles', function() {
	// Run [styles] task when a .scss file is changed
	gulp.watch('src/**/*.scss', gulp.series(['styles']));
}]));
